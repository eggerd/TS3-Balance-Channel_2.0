2.1.1 - 23.02.2022
------------------
- Fixed missing variable error when calculating caption


2.1.0 - 30.07.2019
------------------
- Added [Bugsnag](https://www.bugsnag.com/) integration for error tracking
- [ts3admin.class](https://github.com/par0noid/ts3admin.class) is now integrated using Composer


2.0.4 - 24.03.2019
------------------
- Changed the default codec for all Balance Channel to `Opus Voice`, because `Speex` has been deprecated with server version 3.7.0
- Fixed "*nickname is already in use*" error on servers running version 3.7.0, which was caused by the new naming convention for query users 
- Updated [ts3admin.class](https://github.com/par0noid/ts3admin.class) to version 1.0.2.5
- Fixed some spelling errors


2.0.3 - 23.09.2018
------------------
- **Issue** #6: Changed existing documentation to DocBlock format and added documentation for `debug.php`
- Balance-Channel is now licensed freely under the MIT license
- Added readme with description of the project, requirements, installation instructions etc.
- Some minor code refactoring


2.0.2 - 16.07.2016
------------------
- Initial GitLab version
