<?php

	$TS3BC_SERVER = array(); // storage for all configured ts3server-objects

	require_once('./vendor/autoload.php');
	require_once('./includes/debug.php');
	require_once('./includes/classes/server.php');
	require_once('./configs/configure.php');
	require_once('./includes/bugsnag.php');

	$DEBUG['DATA']['RUNTIME_START_TIME'] = round(microtime(true) * 1000);
	debug(2, '----- start of script -----');

	// executing the script for each ts3server-object
	foreach ($TS3BC_SERVER as $server)
	{
		$server->execute();
	}

	debug_next_tab(0, false);
	debug(2, '----- end of script ('.(round(microtime(true) * 1000) - $DEBUG['DATA']['RUNTIME_START_TIME']).'ms)-----<br><br><br>');

?>