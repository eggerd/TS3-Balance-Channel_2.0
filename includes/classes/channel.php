<?php

require_once(realpath(dirname(__FILE__).'/balance-channel.php'));
// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet


/**
 * Represents a target-channel (or parent-channel) on the TS3 server, whose amount of empty
 * sub-channel is monitored and Balance Channel will be created if there aren't enough free
 * channel available
 */
class ts3bc_channel
{
	// default configuration for all new ts3bc_channel-objects.
	private $CONFIG = array
	(
		// the id of this channel
		'CHANNEL_ID' => null, // :integer [mandatory]

		// the name of this channel, only for human readability (debug output)
		'CHANNEL_NAME' => null, // :string [optional]

		// minimum amount of empty sub-channel, before new balance channel will be added
		'CHANNEL_EMPTY' => 1 // :integer [mandatory]
	);

	// default channel properties
	private $BC_CHANNEL_PROPERTIES = array
	(
		'channel_codec' => ts3admin::CODEC_OPUS_VOICE,
		'channel_codec_quality' => 10,
		'channel_maxclients' => -1,
		'channel_flag_maxclients_unlimited' => 1,
		'channel_maxfamilyclients' => -1,
		'channel_flag_maxfamilyclients_unlimited' => 0,
		'channel_flag_maxfamilyclients_inherited' => 1
	);

	// default channel permissions
	private $BC_CHANNEL_PERMISSIONS = array
	(
		// currently no default values
	);

	/**
	 * Storage for variables that will be filled during runtime
	 * @var array $RUNTIME
	 */
	private $RUNTIME = array
	(
		// storage for all created "balance channel"-objects
		'BALANCE_CHANNEL' => array(),

		// counter for empty sub-channel in this channel
		'CHANNEL_EMPTY' => null,

		// storage for the server object
		'SERVER' => null,

		// storage for the query object
		'QUERY' => null,

		// storage for the database object
		'DB' => null
	);



	/**
	 * Constructor
	 *
	 * @param integer $channel_id = the id of the targeted parent channel used by the TS3 server
	 * @param integer $custom_amount = the amount of empty channel this target channel should have
	 * @param array $custom_properties = associative array with channel properties that should be set for Balance Channels (e.g. 'channel_codec' => 2)
	 * @param array $custom_permissions = associative array with channel permissions that should be set for Balance Channels (e.g. 'i_channel_join_power' => 75)
	 */
	public function __construct($channel_id, $custom_amount = null, array $custom_properties = null, array $custom_permissions = null)
	{
		if(is_int($custom_amount))
		{
			$this->CONFIG['CHANNEL_EMPTY'] = $custom_amount;
		}

		if($custom_properties != null)
		{
			$this->BC_CHANNEL_PROPERTIES = array_merge($this->BC_CHANNEL_PROPERTIES, $custom_properties);
		}

		if($custom_permissions != null)
		{
			$this->BC_CHANNEL_PERMISSIONS = array_merge($this->BC_CHANNEL_PERMISSIONS, $custom_permissions);
		}

		$this->CONFIG['CHANNEL_ID'] = $channel_id;
	}



	/**
	 * Analyzes the channel by counting all empty sub-channel and searching for existing
	 * Balance Channel (and creating their associated objects).
	 *
	 * @param array $channel_list = ts3admin->channelList()
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	public function analyse(array $channel_list)
	{																																		debug(2, 'target channel #'.$this->CONFIG['CHANNEL_ID']); debug_next_tab(1);
		$sub_channel = array(); // storage for channel ids of all sub-channels
		$empty_channel = array();

		foreach ($channel_list as $channel) // for each listed channel
		{
			if($this->CONFIG['CHANNEL_ID'] == $channel['pid']) // only if channel belongs to this target-channel
			{
				$sub_channel[] = $channel['cid'];

				if($channel['total_clients'] == 0) // if channel is empty
				{																															debug(4, 'channel #'.$channel['cid'].' is empty');
					$this->RUNTIME['CHANNEL_EMPTY']++;
					$empty_channel[] = $channel['cid'];
				}
				else
				{
					debug(4, 'channel #'.$channel['cid'].' is occupied');
				}
			}
		}

		$channel_meta = $this->RUNTIME['SERVER']->fetch_channel_topic($sub_channel);
		if($channel_meta['success'])
		{																																	debug(4, 'searching for balance channel'); debug_next_tab(1);
			while($channel_topic = $channel_meta['data']->fetch_object())
			{
				if(preg_match('/Balance Channel/', $channel_topic->value) > 0) // if channel topic contains "Balance Channel"
				{
					debug(2, 'channel #'.$channel_topic->id.' is a balance channel');
					$bc_caption_id = intval(preg_split('/::/', $channel_topic->value)[1]); /* get caption id */								debug_next_tab(1);
					$this->RUNTIME['SERVER']->caption_announce($bc_caption_id); /* announce caption to server */							debug_next_tab(-1);
					$this->RUNTIME['BALANCE_CHANNEL'][] = new ts3bc_balance($this->RUNTIME['SERVER'], $this->RUNTIME['QUERY'], $this->RUNTIME['DB'], $bc_caption_id, is_int(array_search($channel_topic->id, $empty_channel)), $channel_topic->id); // create bc-object
				}
			}

			if(count($this->RUNTIME['BALANCE_CHANNEL']) == 0)
			{
				debug(4, 'no balance channel found');
			}																																debug_next_tab(-1);
		}																																	debug(2, 'empty channel: '.$this->RUNTIME['CHANNEL_EMPTY'].' / balance channel: '.count($this->RUNTIME['BALANCE_CHANNEL'])); debug_next_tab(-2);

		return array('success' => true, 'errors' => null, 'data' => true);
	}



	/**
	 * Deletes unnecessary Balance Channels and releases their caption. Balance Channel with the
	 * highest caption id will be preferred
	 *
	 * @return array {'success' => boolean, 'errors' => null, 'data' => boolean}
	 */
	public function cleanup()
	{																																		debug(2, 'target channel #'.$this->CONFIG['CHANNEL_ID'], 1);
		$dispensable = $this->RUNTIME['CHANNEL_EMPTY'] - $this->CONFIG['CHANNEL_EMPTY']; // amount of unnecessary Balance Channel
		if($dispensable > 0)
		{																																	debug(2, 'dispensable channel: '.$dispensable, 1);
			for($d = $dispensable; $d > 0; $d--) // for the amount of unnecessary Balance Channel
			{
				for($i = count($this->RUNTIME['BALANCE_CHANNEL']) - 1; $i >= 0; $i--) // for all existing Balance Channel
				{
					if($this->RUNTIME['BALANCE_CHANNEL'][$i]->is_empty()) // if this Balance Channel is empty
					{
						$return = $this->RUNTIME['BALANCE_CHANNEL'][$i]->delete(); // delete Balance Channel
						if($return['success'])
						{
							unset($this->RUNTIME['BALANCE_CHANNEL'][$i]); // delete BC-object
							sort($this->RUNTIME['BALANCE_CHANNEL']);
							$this->RUNTIME['CHANNEL_EMPTY']--;

							break;
						}
					}
				}
			}
		}
		else
		{
			debug(2, 'no dispensable balance channel', 1);
		}																																	debug_next_tab(-2);

		return array('success' => true, 'errors' => null, 'data' => true);
	}



	/**
	 * Creates new Balance Channels, if additional channels are required. Custom channel
	 * properties and permissions, that have been passed as this target-channel has been created,
	 * will be used when creating new Balance Channel
	 *
	 * Errors:
	 * 	e013 = 'failed to create new Balance Channel'
	 * 	inherited <= ts3bc_balance->create()
	 *
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	public function balance()
	{																																		debug(2, 'target channel #'.$this->CONFIG['CHANNEL_ID'], 1);
		$required = $this->CONFIG['CHANNEL_EMPTY'] - $this->RUNTIME['CHANNEL_EMPTY']; // amount of required Balance Channel
		if($required > 0)
		{																																	debug(2, 'required balance channel: '.$required, 1);
			for($r = $required; $r > 0; $r--) // for the amount of required Balance Channel
			{
				$this->RUNTIME['BALANCE_CHANNEL'][] = new ts3bc_balance($this->RUNTIME['SERVER'], $this->RUNTIME['QUERY'], $this->RUNTIME['DB'], null, true);

				$index = count($this->RUNTIME['BALANCE_CHANNEL']) - 1;
				$return = $this->RUNTIME['BALANCE_CHANNEL'][$index]->create($this->CONFIG['CHANNEL_ID'], $this->BC_CHANNEL_PROPERTIES, $this->BC_CHANNEL_PERMISSIONS);
				if($return['success'] === false)
				{
					return array('success' => false, 'errors' => errors(array('e013' => 'failed to create new Balance Channel'), $return['errors']), 'data' => null);
				}
			}
		}
		else
		{
			debug(2, 'no balance channel required', 1);
		}																																	debug_next_tab(-2);

		return array('success' => true, 'errors' => null, 'data' => true);
	}



	// ***** ***** HELPER FUNCTIONS ***** *****

	/**
	 * Saves a reference to the server-, query- and database-object ot make requests at the
	 * server and create or delete channel
	 *
	 * @param ts3bc_server $server = reference to the parent server object
	 * @param ts3admin $query = reference to the query that was created by the server
	 * @param mysqli $db = reference to the mysqli object
	 * @return void
	 */
	public function set_server_relations(ts3bc_server &$server, ts3admin &$query, mysqli &$db)
	{																																		debug(4, 'setting server relations for target channel #'.$this->CONFIG['CHANNEL_ID']);
		$this->RUNTIME['SERVER'] =& $server;
		$this->RUNTIME['QUERY'] =& $query;
		$this->RUNTIME['DB'] =& $db;
	}
}

?>
