<?php

require_once(realpath(dirname(__FILE__).'/channel.php'));
require_once(realpath(dirname(__FILE__).'/../../vendor/autoload.php'));
// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet


/**
 * Represents a TS3 server. Thus, it manages the connection to the TS3 servers query and
 * database, and controls the main flow for creating and removing Balance Channels
 */
class ts3bc_server
{
	// default configuration for all new ts3bc_server-objects. If you want to change some
	// of this settings, you have to pass an associative array on creation of your
	// ts3server-object, which contains the keys that you want to change.
	private $CONFIG = array
	(
		// ***** ***** Teamspeak 3 Server ***** *****

		// the IP address on which your TS3 server is listening
		'TS3_SERVER_IP' => '127.0.0.1', // :string [mandatory]

		// the port number on which your TS3 server is listening
		'TS3_SERVER_PORT' => 9987, // :integer [mandatory]

		// the port number on which the TS3 query is listening
		'TS3_QUERY_PORT' => 10011, // :integer [mandatory]

		// the username of the query user you want to use
		'TS3_QUERY_USERNAME' => 'serveradmin', // :string [mandatory]

		// the password for the query user defined at TS3_QUERY_USERNAME
		'TS3_QUERY_PASSWORD' => null, // :string [mandatory]

		// this nickname will be used if the query user connects to the server
		'TS3_QUERY_DISPLAY_NAME' => 'Server [Balance Channel]', // :string [optional]



		// ***** ***** MySQL ***** *****
		// the database is used to easily find all Balance Channels on a server, to get a list of
		// all used caption, to get a list of permissions for each Balance Channel, and to reset
		// the auto increment for channel id's

		// the IP address on which the MySQL server is listening, that is used by your TS3 server
		'MYSQL_HOSTNAME' => 'localhost', // :string [mandatory]

		// the name of your TS3 server database
		'MYSQL_DATABASE' => null, // :string [mandatory]

		// the username for your MySQL database
		'MYSQL_USERNAME' => null, // :string [mandatory]

		// the password for the MySQL user defined at MYSQL_USERNAME
		'MYSQL_PASSWORD' => null, // :string [mandatory]



		// ***** ***** Balance Channel ***** *****

		// the name for all balance channel that will be created in this server.
		// the channel name will also be expanded by a suffix
		'BC_CHANNEL_NAME' => 'Balance Channel', // :string [mandatory]

		// the description that will be visible if you click on a balance channel
		'BC_CHANNEL_DESCRIPTION' => 'This is a Balance Channel', // :string [optional]

		// the numeric id of the icon that should be used for all balance channel
		'BC_CHANNEL_ICON_ID' => null, // :integer [optional]

		// the maximum amount of possible Balance Channel (701 equals A,B,C,D,...,ZZ)
		'BC_MAXIMUM' => 701, // :integer [mandatory]



		// ***** ***** Debug ***** *****

		// determines how detailed the execution, of this server, should be logged.
		// debugging has to be enabled globally for this to take effect - and it will
		// be limited to the global debug level
		// 0 = off / 1 = only errors / 2 = important events / 3 = more detailed / 4 = verbose
		'DEBUG_LEVEL' => 4, // :integer [optional]
	);

	// storage for all available captions
	private $CAPTION = array
	(
		// assignment of caption id to associated caption
		'RANGE' => array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'),

		// storage for the ids of all used captions
		'ASSIGNED' => array()
	);

	/**
	 * Storage for variables that will be filled during runtime
	 * @var array $RUNTIME
	 */
	private $RUNTIME = array
	(
		// storage for the query object
		'QUERY' => null,

		// storage for the database object
		'DB' => null,

		// storage for all created "target channel"-objects
		'TARGET_CHANNEL' => array()
	);



	/**
	 * Constructor
	 *
	 * @param array $custom_config = associative array with $CONFIG-keys that should be overwritten (e.g 'TS3_SERVER_IP' => '127.0.0.1')
	 */
	public function __construct(array $custom_config)
	{
		$this->CONFIG = array_merge($this->CONFIG, $custom_config); // overwrite $CONFIG-key with passed ones
	}



	/**
	 * Is used to add a target-channel (aka parent-channel) that should be monitored. The amount
	 * of empty channels that should be maintained, as well as properties and permissions
	 * dynamically created channels should have, can be defined as well
	 *
	 * @param integer $channel_id = the id of the targeted parent channel used by the TS3 server
	 * @param integer $custom_amount = he amount of empty channel this target channel should have
	 * @param array $custom_properties = associative array with channel properties that should be set for Balance Channels (e.g. 'channel_codec' => 2)
	 * @param array $custom_permissions = associative array with channel permissions that should be set for Balance Channels (e.g. 'i_channel_join_power' => 75)
	 * @return void
	 */
	public function add_target_channel($channel_id, $custom_amount = null, array $custom_properties = null, array $custom_permissions = null)
	{
		$this->RUNTIME['TARGET_CHANNEL'][] = new ts3bc_channel($channel_id, $custom_amount, $custom_properties, $custom_permissions); // create new target-channel
	}



	/**
	 * Executes the main routine that will analyse the server and create or remove
	 * Balance Channels, if necessary
	 *
	 * Errors:
	 * 	e005 = 'failed to fetch channel list from ts3 server'
	 *
	 * @return void
	 */
	public function execute()
	{
		global $DEBUG;
		$DEBUG['SERVER_LEVEL'] = $this->CONFIG['DEBUG_LEVEL'];																				debug(2, 'execution for '.$this->CONFIG['TS3_SERVER_IP'].':'.$this->CONFIG['TS3_SERVER_PORT']); debug_next_tab(1);

		$return = $this->connect();
		if($return['success'])
		{																																	debug(4, 'fetching channel list from teamspeak instance');
			$channel_list = $this->RUNTIME['QUERY']->channelList(); // requesting channel list from query
			if($channel_list['success'])
			{																																debug(2, 'analyzing teamspeak instance');
				foreach ($this->RUNTIME['TARGET_CHANNEL'] as $channel)
				{																															debug_next_tab(1);
					$channel->set_server_relations($this, $this->RUNTIME['QUERY'], $this->RUNTIME['DB']);
					$channel->analyse($channel_list['data']);
				}																															debug(2, 'cleaning up teamspeak instance');

				foreach ($this->RUNTIME['TARGET_CHANNEL'] as $channel)
				{
					$channel->cleanup();
				}																															debug(2, 'balancing teamspeak instance');

				foreach ($this->RUNTIME['TARGET_CHANNEL'] as $channel)
				{
					$channel->balance();
				}
			}
			else // failed to load channel list
			{
				errors(array('e005' => 'failed to fetch channel list from ts3 server'), $channel_list['errors']);
			}
		}

		$this->trim_mysql_autoincrement();
	}



	// ***** ***** HELPER FUNCTIONS ***** *****

	/**
	 * Returns the value for the requested configuration-keys, that have to be passed as array.
	 * If a requested $CONFIG-key doesn't exist, it will be returned with NULL as value. If the
	 * $CONFIG-keys are passed in a associative array, the array-keys will be used as new key in
	 * the returned array
	 *
	 * Example:
	 * 	array{'BC_CHANNEL_NAME'} will return array{'BC_CHANNEL_NAME' => 'Balance Channel'}
	 * 	array{'name' => 'BC_CHANNEL_NAME'} will return array{'name' => 'Balance Channel'}
	 *
	 *
	 * @param array $request = list of $CONFIG-keys whose value is needed. If a string is used as array-key, this key will be used in the returned array instead of the $CONFIG-key
	 * @return array {string => mixed, ...}
	 */
	public function get_settings(array $request)
	{
		$data = array();

		foreach ($request as $map => $key) // for each requested $CONFIG-key
		{
			if(!is_string($map)) // if $CONFIG-key isn't passed associative, use $CONFIG-key in return array
			{
				$map = $key;
			}

			if(preg_match('/password/i', $key) == 0) // only if no password is requested
			{
				if(isset($this->CONFIG[$key])) // if the requested $CONFIG-key exists in configuration
				{
					$data[$map] = $this->CONFIG[$key];
				}
				else
				{
					$data[$map] = null;
				}
			}
			else
			{
				$data[$map] = null; // for passwords NULL will always be returned
			}
		}

		return $data;
	}



	/**
	 * Searches for the next free caption id and calculates the associated caption. If no free
	 * caption can be found, FALSE will be returned
	 *
	 * Errors:
	 * 	e010 = 'the maximum amount of balance channel has been reached'
	 *
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => null|array{'id' => integer, 'value' => string}}
	 */
	public function caption_acquire()
	{																																		debug(3, 'acquiring caption', 1);
		sort($this->CAPTION['ASSIGNED']);
		$caption = null;

		for($i = 0; $i <= $this->CONFIG['BC_MAXIMUM']; $i++) // for every available caption id
		{
			$index = array_search($i, $this->CAPTION['ASSIGNED']);																			debug_next_tab(1);
			if(!is_numeric($index)) // if caption id seems unused
			{																																debug(4, 'lowest available caption id: '.$i);
				$range = count($this->CAPTION['RANGE']);																					debug(4, 'caption range: '.$range);
				$x = $i;																													debug(4, 'calculating caption'); debug_next_tab(1);

				while($x >= $range) // while rest is greater than available caption range (calculate leading caption-letters)
				{
					$x = intval($x / $range) - 1;																							debug(4, 'rest: '.$x);

					if($x >= $range) // if rest is greater than available caption range
					{
						$caption .= $this->CAPTION['RANGE'][0]; /* add first caption-letter */												debug(4, '=> caption: '.$caption, null, true);
					}
					else
					{
						$caption .= $this->CAPTION['RANGE'][$x]; /* add calculated caption-letter */										debug(4, '=> caption: '.$caption, null, true);
					}
				}

				$caption .= $this->CAPTION['RANGE'][$i % $range]; /* calculate last caption-letter */										debug(4, 'caption: '.$caption, -1); debug_next_tab(-1);
				break;
			}
		}

		if($caption != null) // no caption could be calculated
		{
			$this->caption_announce($i); // announce caption as used
			return array('success' => true, 'errors' => null, 'data' => array('id' => $i, 'value' => $caption));
		}

		return array('success' => false, 'errors' => errors(array('e010' => 'the maximum amount of balance channel has been reached')), 'data' => null);
	}



	/**
	 * Removes the passed caption id from the list of used captions, so that it can be reused
	 * for newly created Balance Channel
	 *
	 * @param integer $caption_id = the numeric representation of the caption
	 * @return void
	 */
	public function caption_release($caption_id)
	{
		$caption_index = array_search($caption_id, $this->CAPTION['ASSIGNED']); // determine index of caption id

		if(is_int($caption_index)) // only if caption id could be found
		{																																	debug(3, 'releasing caption #'.$caption_id);
			unset($this->CAPTION['ASSIGNED'][$caption_index]);
		}
	}



	/**
	 * Is used to mark caption ids as used, after analyzing the server or after creating a new
	 * Balance Channel
	 *
	 * @param integer $caption_id = the numeric representation of the caption
	 * @return void
	 */
	public function caption_announce($caption_id)
	{																																		debug(3, 'announcing caption #'.$caption_id);
		$this->CAPTION['ASSIGNED'][] = $caption_id;
	}



	/**
	 * Gets all channels from the database that have a topic assigned. This is used to easily
	 * identify Balance Channel
	 *
	 * @param array $channels = list of channel id's
	 * @return array {'success' => true, 'errors' => null, 'data' => mysqli->query()}
	 */
	public function fetch_channel_topic(array $channels)
	{																																		debug(4, 'fetching channel meta through mysql');
		if(count($channels) > 0)
		{
			$sql = $this->RUNTIME['DB']->query('select c.id, c.value from channel_properties as c, servers as s where s.server_port = '.$this->CONFIG['TS3_SERVER_PORT'].' && s.server_id = c.server_id && c.id in ('.implode(',', $channels).') && c.ident = "channel_topic"');
		}

		return array('success' => true, 'errors' => null, 'data' => $sql);
	}



	/**
	 * Connects to the TS3 server, authenticates the defined query user and changes the
	 * displayed nickname
	 *
	 * Errors:
	 * 	e001 = 'connection to [TS3_SERVER_IP]:[TS3_QUERY_PORT] failed'
	 * 	e002 = 'authentication for user "[TS3_QUERY_USERNAME]" failed'
	 * 	e003 = 'failed to select server with port [TS3_SERVER_PORT]'
	 * 	e004 = 'nickname could not be changed to "[TS3_QUERY_DISPLAY_NAME]"'
	 * 	e007 = 'connection to database [MYSQL_DATABASE] with [MYSQL_USERNAME]@[MYSQL_HOSTNAME] failed'
	 * 	inherited <= ts3admin->connect()
	 * 	inherited <= ts3admin->login()
	 * 	inherited <= ts3admin->selectServer()
	 * 	inherited <= ts3admin->clientUpdate()
	 *
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	private function connect()
	{																																		debug(3, 'connecting to database '.$this->CONFIG['MYSQL_USERNAME'].'@'.$this->CONFIG['MYSQL_HOSTNAME'].':'.$this->CONFIG['MYSQL_DATABASE']);
		$this->RUNTIME['DB'] = new mysqli($this->CONFIG['MYSQL_HOSTNAME'], $this->CONFIG['MYSQL_USERNAME'], $this->CONFIG['MYSQL_PASSWORD'], $this->CONFIG['MYSQL_DATABASE']);

		if(!$this->RUNTIME['DB']->connect_error)
		{																																	debug(3, 'connecting to teamspeak instance '.$this->CONFIG['TS3_QUERY_USERNAME'].'@'.$this->CONFIG['TS3_SERVER_IP'].':'.$this->CONFIG['TS3_QUERY_PORT']);
			$this->RUNTIME['QUERY'] = new ts3admin($this->CONFIG['TS3_SERVER_IP'], $this->CONFIG['TS3_QUERY_PORT']); // create ts3admin-object

			$return = $this->RUNTIME['QUERY']->connect(); // connect to ts3 server
			if($return['success'])
			{																																debug_next_tab(1); debug(4, 'logging in on teamspeak instance');
				$return = $this->RUNTIME['QUERY']->login($this->CONFIG['TS3_QUERY_USERNAME'], $this->CONFIG['TS3_QUERY_PASSWORD']); // login query user
				if($return['success'])
				{																															debug(4, 'selecting virtual teamspeak server');
					$return = $this->RUNTIME['QUERY']->selectServer($this->CONFIG['TS3_SERVER_PORT']); // select ts3 server by port
					if($return['success'])
					{
						$return = $this->RUNTIME['QUERY']->whoAmI();
						if($return['success'] && $return['data']['client_nickname'] !== $this->CONFIG['TS3_QUERY_DISPLAY_NAME'])
						{																													debug(3, 'changing nickname to "'.$this->CONFIG['TS3_QUERY_DISPLAY_NAME'].'"'); debug_next_tab(-1);
							$return = $this->RUNTIME['QUERY']->clientUpdate(array('client_nickname'=> $this->CONFIG['TS3_QUERY_DISPLAY_NAME'])); // change nickname for query user
							if(!$return['success'])
							{
								return array('success' => false, 'errors' => errors(array('e004' => 'nickname could not be changed to "'.$this->CONFIG['TS3_QUERY_DISPLAY_NAME'].'"'), $return['errors']), 'data' => null);
							}
						}

						return array('success' => true, 'errors' => null, 'data' => true);
					}

					return array('success' => false, 'errors' => errors(array('e003' => 'failed to select server with port '.$this->CONFIG['TS3_SERVER_PORT']), $return['errors']), 'data' => null);
				}

				return array('success' => false, 'errors' => errors(array('e002' => 'authentication for user "'.$this->CONFIG['TS3_QUERY_USERNAME'].'" failed'), $return['errors']), 'data' => null);
			}

			return array('success' => false, 'errors' => errors(array('e001' => 'connection to '.$this->CONFIG['TS3_SERVER_IP'].':'.$this->CONFIG['TS3_QUERY_PORT'].' failed'), $return['errors']), 'data' => null);
		}

		return array('success' => false, 'errors' => errors(array('e007' => 'connection to database "'.$this->CONFIG['MYSQL_DATABASE'].'" with '.$this->CONFIG['MYSQL_USERNAME'].'@'.$this->CONFIG['MYSQL_HOSTNAME'].' failed'), mysqli_error($this->RUNTIME['DB'])), 'data' => null);
	}



	/**
	 * Creating a lot of new channels and deleting them increases the MySQL auto increment
	 * counter. Which means, that channel ids can become very big, very quickly. To prevent this,
	 * this function will be called after deleting unnecessary Balance Channel and set the
	 * MySQL auto increment counter to the lowest available value. But only if the server is
	 * currently empty, in order to prevent issues with user activities
	 *
	 * Errors:
	 * 	e006 = 'failed to determine the amount of clients online, because the client-list could not be loaded'
	 * 	e008 = 'determination of greatest channel id failed'
	 * 	e009 = 'failed to update MySQL auto-increment'
	 * 	inherited <= mysqli_error()
	 * 	inherited <= ts3admin->clientList()
	 *
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	private function trim_mysql_autoincrement()
	{																																		debug(2, 'trimming mysql autoincrement'); debug_next_tab(1);
		if($this->RUNTIME['QUERY']->isConnected())
		{
			$client_list = $this->RUNTIME['QUERY']->clientList();
			if(!$client_list['success'])
			{
				return array('success' => false, 'errors' => errors(array('e006' => 'failed to determine the amount of clients online, because the client-list could not be loaded'), $client_list['errors']), 'data' => null);
			}

			foreach($client_list['data'] as $client)
			{
				if($client['client_type'] == 0)
				{																															debug(2, 'aborted, because the server is not empty!');
					return array('success' => true, 'errors' => null, 'data' => false);
				}
			}
		}

		$sql = $this->RUNTIME['DB']->query('select max(channel_id) as peak from channels'); // select greatest channel id
		if($sql)
		{
			$row = $sql->fetch_object();																									debug(4, 'greatest channel id: '.$row->peak);
			if($this->RUNTIME['DB']->query('alter table `channels` AUTO_INCREMENT = '.($row->peak + 1))) // set auto increment counter to next possible channel id
			{																																debug(4, 'set auto increment to '.($row->peak + 1)); debug_next_tab(-1);
				return array('success' => true, 'errors' => null, 'data' => true);
			}

			return array('success' => false, 'errors' => errors(array('e009' => 'failed to update MySQL auto-increment'), mysqli_error($this->RUNTIME['DB'])), 'data' => null);
		}

		return array('success' => false, 'errors' => errors(array('e008' => 'determination of greatest channel id failed'), mysqli_error($this->RUNTIME['DB'])), 'data' => null);
	}



	/**
	 * Destructor - disconnects from the TS3 query and the database, if connections have been
	 * established before
	 */
	public function __destruct()
	{
		if(is_object($this->RUNTIME['QUERY']))
		{
			$this->RUNTIME['QUERY']->quit();
		}

		if(is_object($this->RUNTIME['DB']))
		{
			$this->RUNTIME['DB']->close();
		}
	}
}

?>
