<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents a dynamically created channel (aka Balance Channel) on the TS3 server
 */
class ts3bc_balance
{
	// default configuration for all new ts3bc_balance-objects.
	private $CONFIG = array
	(
		// the id of this Balance Channel
		'BC_CID' => null,

		// the id of the used caption
		'BC_CAPTION_ID' => null,

		// if channel seems empty or not
		'BC_IS_EMPTY' => null
	);

	/**
	 * Storage for variables that will be filled during runtime
	 * @var array $RUNTIME
	 */
	private $RUNTIME = array
	(
		// storage for the server object
		'SERVER' => null,

		// storage for the query object
		'QUERY' => null,

		// storage for the database object
		'DB' => null
	);



	/**
	 * Constructor
	 *
	 * @param ts3bc_server $server = reference to the parent server object
	 * @param ts3admin $query = reference to the query that was created by the server
	 * @param mysqli $db = reference to the mysqli object
	 * @param integer $bc_caption_id = numeric representation of the channels caption (see ts3bc_server)
	 * @param boolean $bc_empty = if the channel is currently empty
	 * @param integer $bc_cid = id of the channel used by the TS3 server
	 */
	public function __construct(ts3bc_server &$server, ts3admin &$query, mysqli &$db, $bc_caption_id = null, $bc_empty = false, $bc_cid = null)
	{
		// setting relations
		$this->RUNTIME['SERVER'] =& $server;
		$this->RUNTIME['QUERY'] =& $query;
		$this->RUNTIME['DB'] =& $db;

		// setting configurations
		$this->CONFIG['BC_CAPTION_ID'] = $bc_caption_id;
		$this->CONFIG['BC_IS_EMPTY'] = $bc_empty;
		$this->CONFIG['BC_CID'] = $bc_cid;
	}



	/**
	 * Creates a new Balance Channel and sets the permissions
	 *
	 * Errors:
	 * 	e011 = 'failed to create channel in target channel #[PARENT_CID]'
	 *  e012 = 'failed to acquire channel caption'
	 * 	e014 = 'failed to configure permissions for channel #[BC_CID]'
	 * 	inherited <= ts3bc_server->caption_acquire()
	 * 	inherited <= ts3admin->channelCreate()
	 * 	inherited <= ts3admin->channelAddPerm()
	 *
	 * @param integer $parent_cid = the id of the target channel (parent channel)
	 * @param array $custom_properties = associative array with channel properties that should be set (e.g. 'channel_codec' => 2)
	 * @param array $custom_permissions = associative array with channel permissions that should be set (e.g. 'i_channel_join_power' => 75)
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	public function create($parent_cid, array $custom_properties = null, array $custom_permissions = null)
	{																																		debug(3, 'creating balance channel');
		$channel_caption = $this->RUNTIME['SERVER']->caption_acquire(); // requesting caption from server
		if($channel_caption['success'])
		{
			$this->CONFIG['BC_CAPTION_ID'] = $channel_caption['data']['id'];

			// building channel properties
			$custom_properties = array_merge($custom_properties, $this->RUNTIME['SERVER']->get_settings(array('channel_name' => 'BC_CHANNEL_NAME', 'channel_description' => 'BC_CHANNEL_DESCRIPTION')), array
			(
				'cpid' => $parent_cid,
				'channel_topic' => 'Balance Channel :: '.$this->CONFIG['BC_CAPTION_ID'],
				'channel_flag_permanent' => 1,
				'channel_flag_semi_permanent' => 0,
				'channel_flag_default' => 0
			));
			$custom_properties['channel_name'] .= ' '.$channel_caption['data']['value']; // append caption to channel name

			// building channel permissions
			$custom_permissions = array_merge($custom_permissions, $this->RUNTIME['SERVER']->get_settings(array('i_icon_id' => 'BC_CHANNEL_ICON_ID')), array
			(
				'i_channel_needed_modify_power' => 75
			));

			$return = $this->RUNTIME['QUERY']->channelCreate($this->parameter_cleanup($custom_properties)); // create new channel
			if($return['success'])
			{																																debug(2, 'channel #'.$return['data']['cid'].' created');
				$this->CONFIG['BC_CID'] = $return['data']['cid'];																			debug(4, 'adding channel permissions');

				$return = $this->RUNTIME['QUERY']->channelAddPerm($this->CONFIG['BC_CID'], $this->parameter_cleanup($custom_permissions)); // set permissions for channel
				if($return['success'])
				{																															debug_next_tab(-1);
					return array('success' => true, 'errors' => null, 'data' => true);
				}

				return array('success' => false, 'errors' => errors(array('e014' => 'failed to configure permissions for channel #'.$this->CONFIG['BC_CID']), $return['errors']), 'data' => null);
			}

			return array('success' => false, 'errors' => errors(array('e011' => 'failed to create channel in target channel #'.$parent_cid), $return['errors']), 'data' => null);
		}

		return array('success' => false, 'errors' => errors(array('e012' => 'failed to acquire channel caption'), $channel_caption['errors']), 'data' => null);
	}



	/**
	 * Deletes this Balance Channel from the server and releases the caption that was used by
	 * this channel
	 *
	 * Errors:
	 * 	e015 = 'failed to delete balance channel #[BC_CID]'
	 * 	e016 = 'failed to delete permission of channel #[BC_CID]'
	 *  e017 = 'failed to restore channel permissions'
	 * 	inherited <= ts3admin->channelDelete()
	 *  inherited <= ts3admin->channelDelPerm()
	 *
	 * @return array {'success' => boolean, 'errors' => null|array, 'data' => boolean|null}
	 */
	public function delete()
	{																																		debug(2, 'deleting balance channel #'.$this->CONFIG['BC_CID']);
		/** IMPORTANT NOTE
		 * Teamspeak and/or MySQL seem to have problems, if a channel gets deleted and then
		 * recreated. It would fail to set permissions for the newly created channel and claim
		 * that no changes were made, although the database doesn't hold any permissions for
		 * this channel.
		 *
		 * To prevent this behavior, all channel permissions will be removed _before_ deleting
		 * the channel!
		 */

		$channel_permission = $this->fetch_channel_permissions();																			debug(4, 'removing channel permissions', 1);

		$return = $this->RUNTIME['QUERY']->channelDelPerm($this->CONFIG['BC_CID'], $channel_permission);
		if($return['success'])
		{																																	debug(4, 'removing channel');
			$return = $this->RUNTIME['QUERY']->channelDelete($this->CONFIG['BC_CID'], 0); // delete channel
			if($return['success'])
			{
				$this->RUNTIME['SERVER']->caption_release($this->CONFIG['BC_CAPTION_ID']); /* release caption */							debug_next_tab(-1);

				return array('success' => true, 'errors' => null, 'data' => true);
			}
			else
			{
				$errors = array('e015' => 'failed to delete balance channel #'.$this->CONFIG['BC_CID']);

				$return_perm = $this->RUNTIME['QUERY']->channelAddPerm($this->CONFIG['BC_CID'], $channel_permission); // try to set permissions for channel again
				if(!$return_perm['success'])
				{
					$errors['e017'] = 'failed to restore channel permissions';
				}

				return array('success' => false, 'errors' => errors($errors, $return['errors'], $return_perm['errors']), 'data' => null);
			}
		}

		return array('success' => false, 'errors' => errors(array('e016' => 'failed to delete permission of channel #'.$this->CONFIG['BC_CID']), $return['errors']), 'data' => null);
	}



	// ***** ***** HELPER FUNCTIONS ***** *****

	/**
	 * Returns the caption id that is used by this channel
	 *
	 * @return integer = the caption id
	 */
	public function get_caption_id()
	{
		return $this->CONFIG['BC_CAPTION_ID'];
	}



	/**
	 * Getter for the current channel status (if it's empty or not)
	 *
	 * @return boolean = if the Balance Channel is currently empty
	 */
	public function is_empty()
	{
		return $this->CONFIG['BC_IS_EMPTY'];
	}



	/**
	 * Will remove all entries from the passed array, whose key isn't a string and the value
	 * is neither a string nor an integer. This is used to filter the channels custom properties
	 * and permissions, to prevent invalid keys/values being passed to the TS3 query
	 *
	 * @param array $parameters = array containing query parameters, that should be checked for invalid entries
	 * @return array {string => string|integer}
	 */
	private function parameter_cleanup(array $parameters)
	{																																		debug(4, 'cleaning up parameters');
		$data = array();																													debug_next_tab(1);

		foreach ($parameters as $key => $value)
		{
			if(is_string($key) && (is_string($value) || is_int($value)))
			{
				$data[$key] = $value;
			}
			else
			{
				debug(4, 'discarding parameter "'.$key.'" => "'.$value.'"');
			}
		}																																	debug_next_tab(-1);

		return $data;
	}



	/**
	 * Selects all permissions assigned to this Balance Channel. This is done via SQL in order
	 * to reduce the required amount of commands that get send to the TS3 query (which caused
	 * problems in the past)
	 *
	 * @return array {string}
	 */
	private function fetch_channel_permissions()
	{
		$data = array();
		$server_settings = $this->RUNTIME['SERVER']->get_settings(array('TS3_SERVER_PORT'));

		$sql = $this->RUNTIME['DB']->query('select perm_id from perm_channel as pc, servers as s where s.server_port = '.$server_settings['TS3_SERVER_PORT'].' && s.server_id = pc.server_id && pc.id1 = '.$this->CONFIG['BC_CID']);
		while($row = $sql->fetch_object())
		{
			$data[] = $row->perm_id;
		}

		return $data;
	}
}

?>
