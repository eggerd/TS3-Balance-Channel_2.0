<?php

// default configuration for all debug functions
$DEBUG = array
(
	// global debug level (0 = off / 1 = only errors / 2 = important events / 3 = more detailed / 4 = verbose)
	'LEVEL' => 0,

	// debug level for currently executed server
	'SERVER_LEVEL' => 4,

	// margin that is used for printing the debug entries
	'MARGIN' => '0 0 2.5px 0',

	// contains some runtime data for the debug function
	'DATA' => array
	(
		// the last used multiplier
		'LATEST_TAB' => 0
	)
);



/**
 * Prints formatted (indented, colored) debug messages according to the global $DEBUG['LEVEL]
 *
 * @param integer $level = at which debug level this message will be printed; must be greater 0
 * @param string $message = the actual debug message
 * @param integer $tab = amount of indents appended before the message, relative to the last used indent value
 * @param boolean $append = if this message should be appended to the previous message
 * @param string $color = color of the message; can be {red, blue, green, orange}
 * @return void
 */
function debug($level, $message, $tab = null, $append = false, $color = null)
{
	global $DEBUG;

	if($level > 0) // only levels greater than zero are accepted, since 0 = off
	{
		if($DEBUG['SERVER_LEVEL'] > $DEBUG['LEVEL'])
		{
			$DEBUG['SERVER_LEVEL'] = $DEBUG['LEVEL'];
		}

		// only if the debug message lies inside the global debug level or the current servers debug level
		if($level <= $DEBUG['LEVEL'] && $level <= $DEBUG['SERVER_LEVEL'])
		{
			switch($color)
			{
				case 'red': $color = '#FF3838'; break;
				case 'blue': $color = '#6666FF'; break;
				case 'green': $color = '#34A953'; break;
				case 'orange': $color = '#FBBC05'; break;
				default: $color = '#000000'; break;
			}


			if($append) // if this message should be appended to the previous message
			{
				$tab = null; // indents are ignored
			}
			else
			{
				if($tab === null || is_nan($tab)) // if the previous indent value should be used or a invalid value was passed
				{
					$tab = $DEBUG['DATA']['LATEST_TAB'];
				}
				else
				{
					if($tab != 0)
					{
						$tab = $DEBUG['DATA']['LATEST_TAB'] + $tab; // calculating the new indent value, based on the last used value
					}

					$DEBUG['DATA']['LATEST_TAB'] = $tab;
				}

				echo '<br>';
			}


			$margin = 15 * $tab; // convert indents into CSS margin

			echo '<div style="display: inline-block">';
				if(!$append){echo '<p style="display: inline-block; margin: '.$DEBUG['MARGIN'].'; vertical-align: top">'.debug_time().'&nbsp;</p>';}
				echo '<p style="display: inline-block; margin: 0 0 0 '.($margin < 0 ? 0 : $margin).'px; color: '.$color.';">'.$message.'&nbsp;</p>
			</div>';
		}
		else
		{
			if(!$append)
			{
				$DEBUG['DATA']['LATEST_TAB'] += --$tab;
			}
		}
	}
}



/**
 * Takes one or multiple error message(s) as part of an array, and writes them to stderr. Afterwards,
 * all passed errors are returned in a single array. If they had a string as key, this string will
 * be appended to the message
 *
 * @param array ...$errors = error messages from one or multiple arrays
 * @return array = all passed errors combined in one array
 */
function errors(...$errors)
{
	global $bugsnag;
	$return = array();
	$stderr = fopen('php://stderr', 'w');

	foreach ($errors as $message)
	{
		if(is_array($message))
		{
			foreach ($message as $key => $value)
			{
				$message = (is_string($key) ? $key.': ' : null).$value;
				$return[] = $message;

				debug(1, $message, null, false, 'red');
				fwrite($stderr, debug_time().$message."\n");

				if(!empty(BUGSNAG_API_KEY) && isset($bugsnag)) {
					$bugsnag->notifyError('HandledError', $message);
				}
			}
		}
	}

	fclose($stderr);
	return $return;
}



/**
 * Converts microtime into a human readable format. Will be appended to every debug message
 *
 * @return string = human readable microtime for the debug output
 */
function debug_time()
{
	return '['.date('d-m-Y H:i:s').':'.(sprintf("%04d", round(microtime(true) * 1000) - (time() * 1000))).'] ';
}



/**
 * Allows to quickly overwrite the last used indent value. Either by directly overwriting it
 * with the passed value, or by adding it on top of it
 *
 * @param integer $tab = amount of indents that will be appended before the message
 * @param boolean $relative = add value (TRUE) instead of overwriting (FALSE)
 * @return void
 */
function debug_next_tab($tab, $relative = true)
{
	global $DEBUG;

	if(is_int($tab))
	{
		$DEBUG['DATA']['LATEST_TAB'] += $tab;

		if(!$relative)
		{
			$DEBUG['DATA']['LATEST_TAB'] = $tab;
		}
	}
}

?>