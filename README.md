# TS3-Balance-Channel 2.0

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F3272349%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/releases)
[![pipeline status](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/badges/master/pipeline.svg)](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=ts3-balance-channel&metric=alert_status)](https://sonarcloud.io/dashboard?id=ts3-balance-channel) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=ts3-balance-channel&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=ts3-balance-channel)

Balance-Channel allows you to dynamically increase the amount of channels on your server. If the amount of free channels drops below a configured threshold, the script will create new channel until the threshold is reached again and will remove them, as soon as they are no longer needed. 

This guarantees each user a free channel, without having to increase the size of the channel tree permanently and thereby making it unnecessarily confusing.

The threshold, as wells as channel settings (e.g. codec) and permissions of dynamically created channels, can be configured independently for each monitored parent channel. Moreover, the script can be used for multiple TS3 servers simultaneously and every server can be configured independently.

In short:
- Dynamically creates & removes channels, depending on the amount of free channels available
- Keeps the channel tree as small as possible while guaranteeing free channels on the server
- Channel settings (e.g. codec) and permissions of Balance-Channel can be configured independently for each monitored parent channel
- Can be used for multiple TS3 servers simultaneously & each server can be configured independently


## Demo

You can see the script in action on my own public [Teamspeak 3 server](ts3server://eggerd.de), where it will create channel called `Balance Laberecke`, if there aren't enough free `Laberecke` channels available. 

> Note that the script kicks into action mostly during evening/night times, when most users are online.

## Requirements

- PHP 5.6.0 or higher
- Teamspeak Server 3.2.0 or higher
- Read access to the database of the Teamspeak 3 server, for at least [those tables](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis/home#teamspeak-3-database)
- Access to the Teamspeak 3 query over Telnet, with a query account that has at least [these permissions](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis/home#teamspeak-3-query)
- The topic of channels should only be editable by administrators, to [prevent abuse](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis/home#teamspeak-3-channel-topic)
- Ability to schedule cronjobs
- [Composer](https://getcomposer.org/), to install dependencies

## Installation

1. Download the [latest version](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/releases)
1. Run `composer install` in the root directory of the project
1. Edit the [configuration](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis/home#available-settings) files in `configs/` according to your needs
1. Upload all files to your server
1. [Set up a cronjob](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis/home#cronjob) that executes the `index.php`

> The script accesses the Teamspeak 3 query over Telnet. Therefore, traffic between the script and the TS3 query is not encrypted! Due to this, it is recommended that the script is executed on the same machine as the TS3 server itself.

## Documentation

A documentation with more details can be found in the [wiki](https://gitlab.com/eggerd/TS3-Balance-Channel_2.0/-/wikis) of the repository.

## Licensing

This software is available freely under the MIT License.  
Copyright (c) 2018 Dustin Eckhardt