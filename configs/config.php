<?php

/**
 * Teamspeak 3 Balance Channel
 * Copyright (c) 2018 Dustin Eckhardt
 *
 * https://gitlab.com/eggerd/TS3-Balance-Channel_2.0
 */
config::set('VERSION', '2.1.0');

config::set(config::secret('TS3_USERNAME'));
config::set(config::secret('TS3_PASSWORD'));
config::set(config::secret('MYSQL_USERNAME'));
config::set(config::secret('MYSQL_PASSWORD'));
config::set('MYSQL_DATABASE', 'ts3_server');


/***** SERVER *****
 * It is possible to handle multiple Teamspeak servers at once. For each server you want
 * to manage, you have to create a new ts3bc_server object and store it in $TS3BC_SERVER[INDEX],
 * where you have to replace INDEX with a unique number.
 * For example:
 * 		$TS3BC_SERVER[0] = new ts3bc_server(...); // Server 1
 * 		$TS3BC_SERVER[1] = new ts3bc_server(...); // Server 2
 *
 * To change some of the default server settings, you have to pass the associated keys
 * in an associative array as the first parameter the ts3bc_server object.
 * For example:
 * 		$TS3BC_SERVER[0] = new ts3bc_server(array('TS3_QUERY_PASSWORD' => 'passwd123', 'BC_CHANNEL_NAME' => 'Test'));
 *
 * You can find all possible configuration keys and their meaning in /includes/classes/server.php,
 * or in the configuration example below!
 *
 * Syntax:
 * 		$TS3BC_SERVER[{INDEX}] = new ts3bc_server({CUSTOM_CONFIG});
 * 		// {INDEX}			: mandatory = continuous numerating for each server, starting by 0
 * 		// {CUSTOM_CONFIG} 	: optional 	= expects a associative array - syntax: 'CONFIG_KEY' => 'new value', 'CONFIG_KEY2' => 5
 */



/***** CHANNEL *****
 * After creating your server you have to add channels of your TS3 server, that you want
 * to be monitored. Those channels have to be parent channel for other channel.
 * Example channel tree:
 * 		(channel id)	(channel name)
 * 		#1 				|- Channel 1
 * 		#2				|	|- Sub-Channel 1.1
 * 		#3				|	|- Sub-Channel 1.2
 * 		#4 				|- Channel 2
 * 		#5 				|- Channel 3
 * 		#6					|- Sub-Channel 3.1
 * 		#7						|- Sub-Channel 3.1.1
 *
 * If you look at the example above, channel #1, #5 and #6 are suitable. But
 * channel #2, #3, #4 and #7 are inappropriate, because they don't have any sub channel.
 * For example:
 *		$TS3BC_SERVER[0]->add_target_channel(6);
 *		// This would add channel #6 to the ts3bc_server object, that you previously created
 *		// and stored in $TS3BC_SERVER[0].
 *
 * By default this program will add new channel to your target channel, if there are no empty
 * sub-channel. But you can change this behavior by passing the preferred amount of empty
 * channel as the second parameter.
 * For example:
 * 		$TS3BC_SERVER[0]->add_target_channel(6, 3);
 * 		// This would monitor channel #6 and ensure that there will always be 3 empty channel.
 *
 * If you want to change or add some TS3 channel properties, you can pass those properties in
 * an associative array as the third parameter.
 * Please note, that you can't define a channel topic, because this is used to identify Balance
 * Channel. Also, Balance Channel will always be permanent channels and never be a default channel!
 * For example:
 * 		$TS3BC_SERVER[0]->add_target_channel(6, null, array('channel_codec' => 1, 'channel_maxclients' => 10));
 * 		// For each created Balance Channel in channel #6, the codec would be set to 1 and the
 * 		// maximum amount of clients to 10. Note, that the amount of preferred empty channel
 * 		// still uses the default value (1), because NULL was passed as the second parameter.
 *
 * It is also possible to configure desired permissions for your Balance Channel. You can pass
 * those permission names/ids in an associative array as the fourth parameter. Please note, that
 * modifying a Balance Channel should only be possible for query server admins.
 * For example:
 * 		$TS3BC_SERVER[0]->add_target_channel(6, null, null, array('i_ft_needed_file_browse_power' => 75));
 * 		// This would restrict access to the file browser for all Balance Channel that will be
 * 		// created in channel #6. Preferred amount of empty channel and channel properties still
 * 		// use the default value, because NULL was passed as the second and third parameter.
 *
 * Syntax:
 *		$TS3BC_SERVER[{INDEX}]->add_target_channel({CHANNEL_ID}, {EMPTY_CHANNEL}, {PROPERTIES}, {PERMISSIONS});
 *		// {INDEX}			: mandatory = the index of your server, to which you want to add this channel
 *		// {CHANNEL_ID}		: mandatory = the numeric id of your parent channel
 *		// {EMPTY_CHANNEL} 	: optional 	= the amount of preferred empty channel in your parent channel
 *		// {PROPERTIES}		: optional 	= expects a associative array with TS3 channel properties - syntax: 'property_name' => 'value', 'property_name2' => 5
 *		// {PERMISSIONS}	: optional 	= expects a associative array with TS3 permission names/ids - syntax: 'i_ft_needed_file_browse_power' => 75, 'i_channel_needed_join_power' => 75
 */



/***** DEBUGGING *****
 * There exists a global debug level and a debug level for each single server object. The debug
 * level of a server can only be as high as the global debug level - therefor, debugging has to
 * be enabled globally for servers to take effect.
 * Available Debug Level:
 * 		#0 = off
 * 		#1 = only errors
 * 		#2 = important events
 * 		#3 = more detailed
 * 		#4 = verbose
 *
 * For example:
 * 		config::set('DEBUG', ['LEVEL' => 3]);
 * 		// This would set the global debug level to #3
 *
 * Moreover you can adjust the debug level for each server individually. So if you have three servers,
 * but only want debug information for one of them, you can adjust the debug level of the other
 * two servers accordingly.
 * For example:
 * 		$TS3BC_SERVER[0] = new ts3bc_server(array('DEBUG_LEVEL' => 0));
 * 		// This would turn off debugging for this server
 *
 * Servers will be created with a debug level of #4 by default. So if you turn on debugging
 * globally, each created server will print debug output (according to the global debug level!).
 */



/***** FINAL NOTE *****
 * This program uses the TS3 channel topic to easily identify Balance Channel on a TS3 Server.
 * To prevent abuse, normal users should not be allowed to create channel with topics.
 */



// example configuration used for my own TS3 server
$TS3BC_SERVER[0] = new ts3bc_server(array
(
	'TS3_QUERY_USERNAME' => config::get('TS3_USERNAME')['value'],
	'TS3_QUERY_PASSWORD' => config::get('TS3_PASSWORD')['value'],
	'TS3_QUERY_PORT' => 30033,

	'MYSQL_DATABASE' => config::get('MYSQL_DATABASE')['value'],
	'MYSQL_USERNAME' => config::get('MYSQL_USERNAME')['value'],
	'MYSQL_PASSWORD' => config::get('MYSQL_PASSWORD')['value'],

	'BC_CHANNEL_ICON_ID' => '1628017055',
	'BC_CHANNEL_NAME' => '|» Balance Laberecke',
	'BC_CHANNEL_DESCRIPTION' => '[COLOR=#E18000][B]Balance Channel[/B][/COLOR]\n\nChannel dieser Art werden vom Server automatisch erstellt, sollten nicht mehr genügend freie Channel in einem Bereich verfügbar sein.\n\nSomit bleibt der Channel-Baum so kompakt und übersichtlich wie möglich, während jedem ein eigener Channel gewährleistet werden kann.\n\n[right][ [url=https://ts.eggerd.de/?s=server-translation&c=7]View English Translation[/url] ][/right]'
));
$TS3BC_SERVER[0]->add_target_channel(4, 3); // Public Channels
$TS3BC_SERVER[0]->add_target_channel(13, null, null, array // Channel for regulars
(
	'i_channel_needed_join_power' => 35,
	'i_ft_needed_file_upload_power' => 35,
	'i_ft_needed_file_download_power' => 35,
	'i_ft_needed_file_delete_power' => 35,
	'i_ft_needed_file_rename_power' => 35,
	'i_ft_needed_file_browse_power' => 35,
	'i_ft_needed_directory_create_power' => 35
));
$TS3BC_SERVER[0]->add_target_channel(18, 1, null, array // VIP-Area
(
	'i_channel_needed_join_power' => 65,
	'i_ft_needed_file_upload_power' => 65,
	'i_ft_needed_file_download_power' => 65,
	'i_ft_needed_file_delete_power' => 65,
	'i_ft_needed_file_rename_power' => 65,
	'i_ft_needed_file_browse_power' => 65,
	'i_ft_needed_directory_create_power' => 65
));

config::set('DEBUG', ['LEVEL' => 1]);
config::set('TS3BC_SERVER', $TS3BC_SERVER);



/***** Error Tracking *****/

// API key for the Bugsnag project (leave empty to disable tracking)
config::set('BUGSNAG_API_KEY', ''); // :string

// List of environment names that will be tracked by Bugsnag
config::set('BUGSNAG_TRACK_ENVIRONMENTS', ['prod', 'test']); // :array

?>